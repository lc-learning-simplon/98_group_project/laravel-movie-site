<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use App\Movie;

class CartController extends Controller
{
    public function index(Request $request) 
    {
        if ($request->session()->exists('cart')) {
            return view('cart', ['movies' => Movie::all(), 'cart' => $request->session()->get("cart"), 'total' => count($request->session()->get("cart"))]);
        }
        else {
            return view('cart');
        }
    }

    public function addToCart(Request $request, Redirector $redirect)
    {
        // $_SESSION["cart"][$request->input("id")] = $request->input("id");
        $request->session()->push('cart', $request->input("id"));
        return $redirect->to('/home');
    }

    public function reset(Request $request, Redirector $redirect) {
        $request->session()->pull('cart');
        return $redirect->to('/home');
    }


}
