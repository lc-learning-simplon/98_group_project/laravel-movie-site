<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Order;
use App\Movie;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class ProfilController extends Controller
{
    public function index()
    {

        $orders = User::find(Auth::id())->order()->get();
        $movies = [];

        foreach ($orders as $order) {

            $movies_id = DB::table('movie_order')->where('order_id', $order->id)->get('movie_id');

            foreach ($movies_id as $movie_id) {

                $movies[] = Movie::find($movie_id->movie_id);
            }
        }


        return view('profil', ['user' => Auth::user(), 'movies' => $movies]);
    }
}
