<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Movie;
use Illuminate\Support\Facades\Auth;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Redirect;

class MovieController extends Controller
{
    public function show($id)
    {
        return view('movie', ["movie" => Movie::find($id)]);
    }

    // Modifié un film
    public function update(Request $request, Redirector $redirect)
    {
        $movie = Movie::find($request->input('id'));

        $movie->resume = $request->input('resume');
        $movie->year = $request->input('year');
        $movie->released = $request->input('released');
        $movie->runtime = $request->input('runtime');
        $movie->genre = $request->input('genre');
        $movie->director = $request->input('director');
        $movie->language = $request->input('language');
        $movie->country = $request->input('country');
        $movie->awards = $request->input('awards');
        $movie->poster = $request->input('poster');
        $movie->Type = $request->input('Type');
        $movie->DVD = $request->input('DVD');
        $movie->Production = $request->input('Production');
        $movie->save();

        return $redirect->to('/home');
    }

    public function delete($id, Redirector $redirect)
    {

        $movie = Movie::find($id);
        $movie->delete();

        return $redirect->to('/home');
    }
}
