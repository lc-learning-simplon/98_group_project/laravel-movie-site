<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use App\Http\Resources;
use App\Http\Resources\MovieCollection;
use App\Movie;



class AddmovieController extends Controller
{
    public function index(Request $request) 
    { 
        if (!null == $request->input('title')) {
            $datajson = file_get_contents("http://www.omdbapi.com/?apikey=" . env("API_KEY") . "&s=" . $request->input('title') . "&"); // Récupération de l'api au format json.
            $data = json_decode($datajson);
            return view('addmovie', ['movies' => $data]); // Envoie des données à la vue
        }

        else {
            return view('addmovie');
        } 
    }
    public function insert(Request $request, Redirector $redirect){
        $datajson = file_get_contents("http://www.omdbapi.com/?apikey=".env('API_KEY')."&i=" . $request->input('id') . "&"); // Récupération de l'api au format json.
        $data = json_decode($datajson);
        $movie=new Movie;
        $movie->title=$data->Title;
        $movie->resume=$data->Plot;
        $movie->year=$data->Year;
        $movie->released=$data->Released;
        $movie->runtime=$data->Runtime;
        $movie->genre=$data->Genre;
        $movie->director=$data->Director;
        $movie->language=$data->Language;
        $movie->country=$data->Country;
        $movie->awards=$data->Awards;
        $movie->poster=$data->Poster;
        $movie->Type=$data->Type;
        $movie->DVD=$data->DVD;
        $movie->Production=$data->Production;  
        $movie->avalaible=1;
        $movie->save();
        return $redirect->to('/home');
        
    }
    

}

