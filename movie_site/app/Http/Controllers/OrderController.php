<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Movie;
use App\Order;

class OrderController extends Controller
{
    public function addToOrder(Request $request, Redirector $redirect) {

        $order = new Order;
        $order->reference = uniqid();
        $order->user_id = Auth::id();
        $order->save();

        $cart = $request->session()->get("cart");
        $movies = Movie::all();

        foreach ($cart as $id) {
            DB::table('movie_order')->insert([
                'order_id' => $order->id,
                'movie_id' => $id
            ]);
            $movie = Movie::find($id);
            $movie->avalaible = 0;
            $movie->save();
        }

        return $redirect->to('/reset');
    }

    public function render (Request $request, Redirector $redirect) {
        $movie = Movie::find($request->input('id'));
        $movie->avalaible = 1;
        $movie->save();

        DB::table('movie_order')->where('movie_id', $movie->id)->delete();
    

        return $redirect->back();
    }
}
