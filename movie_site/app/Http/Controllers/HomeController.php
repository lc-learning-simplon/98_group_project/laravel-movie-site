<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Movie;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;



class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home', ['movies' => Movie::all()]);
    }


    // Option de trie 
    public function select(Request $request, Redirector $redirect){

        // Control
        if ($request->input('trie') == 'avalaible') {
            //cas 1
            $movies = DB::table('movies')->orderByRaw($request->input('trie') . ' DESC')->get();
    
            return view('/home', ["movies" => $movies]);
        }
  
        else {
            //cas 2
            $movies = DB::table('movies')->orderByRaw($request->input('trie'))->get();
    
            return view('/home', ["movies" => $movies]);
        }
    }


    // Barre de recherche Film par motClé
    public function search(Request $request, Redirector $redirect){

        $movies = DB::table('movies')
            ->where('title', 'like', '%' . $request->input('motCle') . '%')
            ->orWhere('resume', 'like', '%' . $request->input('motCle') . '%')
            ->get();
        return view('result', ['movies' => $movies]);
    }
    
}
