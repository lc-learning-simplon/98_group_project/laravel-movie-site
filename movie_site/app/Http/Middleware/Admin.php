<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()) {

            if (isset(Auth::user()->role()->first()->name)) {
                return $next($request);
            } 
            else {
                return redirect('/home')->with('warning', 'Accés non autorisé, vous avez été redirigé.');
            }
        } 
        else {
            return redirect('/home');
        }
    }
}
