<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMoviesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->char('title',255);
            $table->longText('resume');
            $table->char('year',255);
            $table->char('released',255);
            $table->char('runtime',255);
            $table->char('genre',255);
            $table->char('director',255);
            $table->char('language',255);
            $table->char('country',255);
            $table->char('awards',255);
            $table->longText('poster');
            $table->char('Type',255);
            $table->char('DVD',255);
            $table->char('Production',255);
            $table->boolean('avalaible');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movies');
    }
}
