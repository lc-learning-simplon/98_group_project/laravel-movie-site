@extends('layouts.app')

@section('content')


<div class="container">

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-white">
            <li class="breadcrumb-item"><a href="/home">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{$user->name}}</li>
        </ol>
    </nav>

    <h1>Profil</h1>
    <hr>

    <h4><strong>Nom :</strong> {{$user->name}}</h4>
    <h4><strong>E-mail :</strong> {{$user->email}}</h4>

    @if ($user->role->first->name)
    <br>
    <h3>{{$user->role->first()->name}}</h3>
    @endif

    <hr>
    <h2>Vos commandes</h2>

    <ul class="list-group col-md-5 mx-left">
        @foreach($movies as $movie)

        <li class="list-group-item d-flex justify-content-between align-items-center">
            {{$movie->title}}
            <form action="/render" method="post">
                @csrf
                <input type="hidden" name="id" value="{{$movie->id}}">
                <button type="submit" class="btn badge badge-info badge-pill">Rendre</button>
            </form>
        </li>
        @endforeach
    </ul>


</div>

@endsection