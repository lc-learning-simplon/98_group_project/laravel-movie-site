@extends('layouts.app')

@section('content')

<div class="container">

	<nav aria-label="breadcrumb">
		<ol class="breadcrumb bg-white">
			<li class="breadcrumb-item"><a href="/home">Home</a></li>
			<li class="breadcrumb-item active" aria-current="page">Panier</li>
		</ol>
	</nav>

	@if(isset($cart))
	<h1>Votre panier :</h1>

	<table class="table table-striped">
		<thead>
			<tr scope="row">
				<th scope="col">Titre</th>
				<th scope="col">Durée</th>
				<th scope="col">Auteur</th>
				<th scope="col">Prix</th>
			</tr>
		</thead>
		<tbody>
			@foreach($cart as $id)

			<tr scope="row">
				<td scope="col">{{$movies[$id-1]->title}}</td>
				<td scope="col">{{$movies[$id-1]->runtime}}</td>
				<td scope="col">{{$movies[$id-1]->director}}</td>
				<td scope="col">5 €</td>
			</tr>
			@endforeach
		</tbody>
		<tfoot>
			<tr scope="row">
				<th scope="col">Titre</th>
				<th scope="col">Durée</th>
				<th scope="col">Auteur</th>
				<th scope="col">Prix</th>
			</tr>
		</tfoot>
	</table>
	<hr>

	<h4>Total : {{$total*5}} &euro;</h4>
	<h4>Nombre de films : {{$total}}</h4>
	<div>
		<a class="btn btn-outline-primary" href="/home">Retour à l'accueil</a>
		<a class="btn btn-success" href="/addorder">Valider la commande</a>
		<a class="btn btn-outline-danger" href="/reset">Vider le panier</a>
	</div>

	@else
	<h1>Votre panier est vide</h1>
	<a class="btn btn-outline-primary" href="/home">Retour à l'accueil</a>
	@endif

</div>

@endsection