@extends('layouts.app')

@section('content')


<div class="container">

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-white">
            <li class="breadcrumb-item"><a href="/home">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Ajouter un film</li>
        </ol>
    </nav>

    <h1>Ajoutez un film</h1>
    <hr>


    <form method="Get" action="/addmovie">

        <div class="form-group col-md-4 mx-left">
            <label for="title">Titre</label>

            <input name="title" id="title" class="form-control" type="text">
            <button type="submit" class="btn btn-outline-info mt-2">Rechercher</button>
        </div>

    </form>



    @if (isset($movies))
    <h1 class="title">Resulats :</h1>

    <div class="d-flex justify-content-md-between flex-wrap">
        @foreach ($movies->Search as $movie)

        <div class="card mb-4" style="width: 18rem;">
            <img src="{{$movie->Poster}}" class="card-img-top" alt="">
            <div class="card-body">
                <h5 class="card-title">{{$movie->Title}} - {{$movie->Year}}</h5>
            </div>
            <form action="/addtodb" method="POST">
                @csrf
                <input name="id" type="hidden" value="{{$movie->imdbID}}">
                <div class="modal-footer">
                    <button type="submit" class="btn btn-outline-success m-auto">Ajouter</button>
                </div>

            </form>
        </div>

        @endforeach
    </div>

    @endif

</div>






@endsection