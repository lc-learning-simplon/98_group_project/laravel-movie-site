@extends('layouts.app')

@section('content')

@if(isset(Auth::user()->role->first()->name))
<div class="container">

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-white">
            <li class="breadcrumb-item"><a href="/home">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{$movie->title}}</li>
        </ol>
    </nav>

    <div class="card mb-3" style="max-width: 100%;">
        <div class="row no-gutters">
            <div class="col-md-4">
                <img src="{{$movie->poster}}" class="card-img" alt="">
            </div>
            <div class="col-md-8">
                <div class="card-body">
                    <h5 class="card-title">{{$movie->title}} - {{$movie->year}}</h5>
                    <form method="POST" action='/movie/update'>
                        @csrf
                        
                        <input type="hidden" name="id" value="{{$movie->id}}">

                        <label for="resume">Description</label>
                        <textarea name="resume" type="text" class="form-control" id="resume">{{$movie->resume}}</textarea>

                        <label for="year">Annnée</label>
                        <input name="year" value="{{$movie->year}}" type="text" class="form-control" id="year">

                        <label for="release">Date de realisation</label>
                        <input name="released" value="{{$movie->released}}" type="text" class="form-control">

                        <label for="runtime">Durée</label>
                        <input name="runtime" value="{{$movie->runtime}}" type="text" class="form-control">

                        <label for="genre">Genre</label>
                        <input name="genre" value="{{$movie->genre}}" type="text" class="form-control">

                        <label for="director">Producteur</label>
                        <input name="director" value="{{$movie->director}}" type="text" class="form-control">

                        <label for="language">Langue</label>
                        <input name="language" value="{{$movie->language}}" type="text" class="form-control">

                        <label for="country">Pays de production</label>
                        <input name="country" value="{{$movie->country}}" type="text" class="form-control">

                        <label for="awards">Prix Awards</label>
                        <input name="awards" value="{{$movie->awards}}" type="text" class="form-control">

                        <label for="poster">Affiche</label>
                        <input name="poster" value="{{$movie->poster}}" type="text" class="form-control">

                        <label for="Type">Type</label>
                        <input name="Type" value="{{$movie->Type}}" type="text" class="form-control">

                        <label for="DVD">Sortie DVD</label>
                        <input name="DVD" value="{{$movie->DVD}}" type="text" class="form-control">

                        <label for="Production">Production</label>

                        <input name="Production" value="{{$movie->Production}}" type="text" class="form-control">

                        <button type="submit" type="button" class="btn btn-info mt-2">Modifier</button>
                    </form>

                    <button id="delete" class="btn btn-danger mt-2" data-id="{{$movie->id}}">Supprimer</button>

                </div>
            </div>
        </div>
    </div>

    @if($movie->avalaible == 1)
    <form action="/addtocart" method="POST">
        @csrf
        <input type="hidden" name="id" value="{{$movie->id}}">
        <div class="btn-group" role="group" aria-label="Basic example">
            <button type="submit" class="btn btn-success">5 €</button>
            <button type="submit" class="btn btn-outline-success">Ajouter au panier</button>
        </div>
    </form>
    @else
    <form action="#" method="">
        <button type="submit" class="btn btn-outline-secondary">Non disponible</button>
    </form>
    @endif

</div>

<!-- Script de confirmation avant suppression -->

<script type="text/JavaScript">
    document.getElementById("delete").addEventListener("click", function() {

        let id = document.getElementById("delete").getAttribute("data-id");

        if (confirm("Êtes-vous sur ?") == false) {
        }
        else {
            window.location.replace("/movie/" + id + "/delete");
        }

        
    });
</script>

@else
<div class="container">

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-white">
            <li class="breadcrumb-item"><a href="/home">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{$movie->title}}</li>
        </ol>
    </nav>

    <div class="card mb-3" style="max-width: 100%;">
        <div class="row no-gutters">
            <div class="col-md-4">
                <img src="{{$movie->poster}}" class="card-img" alt="">
            </div>
            <div class="col-md-8">
                <div class="card-body">
                    <h5 class="card-title">{{$movie->title}} - {{$movie->year}}</h5>
                    <p class="card-text">{{$movie->resume}}</p>
                    <p class="card-text"><small class="text-muted">By {{$movie->director}} - {{$movie->language}} - {{$movie->runtime}}</small></p>
                    <p class="card-tag">{{$movie->genre}}</p>
                </div>
            </div>
        </div>
    </div>

    @if($movie->avalaible == 1)
    <form action="/addtocart" method="POST">
        @csrf
        <input type="hidden" name="id" value="{{$movie->id}}">
        <div class="btn-group" role="group" aria-label="Basic example">
            <button type="submit" class="btn btn-success">5 €</button>
            <button type="submit" class="btn btn-outline-success">Ajouter au panier</button>
        </div>
    </form>
    @else
    <form action="#" method="">
        <button type="submit" class="btn btn-outline-secondary">Non disponible</button>
    </form>
    @endif

</div>

@endif


@endsection