@extends('layouts.app')

@section('content')

<style>


</style>

<div class="container">

    <div class="row justify-content-md-between mb-5">

        <form action='/home' method="post">
            <div class="input-group col-sm">
                @csrf
                <select class="custom-select" name="trie" id="select">
                    <option selected>Trier par</option>
                    <option value="year">Année de sortie</option>
                    <option value="title">Titre</option>
                    <option value="runtime">Durée</option>
                    <option value="avalaible">Disponibilité</option>
                </select>
                <div class="input-group-append">
                    <button class="btn btn-outline-info" type="submit">Trier</button>
                </div>
            </div>
        </form>


        <form action="/search" method="POST">
            <div class="input-group col-sm">
                @csrf
                <input type="search" class="form-control" placeholder="Rechercher" id="search" name="motCle">
                <div class="input-group-append">
                    <button class="btn btn-outline-info">Rechercher</button>
                </div>
            </div>
        </form>

    </div>


    <div class="d-flex justify-content-md-between flex-wrap">
        @foreach($movies as $movie)
        @if ($movie->avalaible === 1)  
        <div class="card mb-4" style="width: 18rem;">
            <img src="{{$movie->poster}}" class="card-img-top" alt="">
            <div class="card-body">
                <h5 class="card-title">{{$movie->title}} - {{$movie->year}}</h5>
                <p class="card-text">{{$movie->resume}}</p>
            </div>
            <div class="modal-footer">
                <a href="/movie/{{$movie->id}}" class="btn btn-info m-auto">En savoir plus</a>

            </div>
        </div>
        @else
        <div class="card mb-4"style="width: 18rem;">
            <img src="{{$movie->poster}}" class="card-img-top" alt="">
            <div class="card-body">
                <h5 class="card-title">{{$movie->title}} - {{$movie->year}}</h5>
                <p class="card-text">{{$movie->resume}}</p>
            </div>
            <div class="modal-footer">
                <a href="/movie/{{$movie->id}}" class="btn btn-outline-secondary m-auto">Non disponible</a>

            </div>
        </div>
        @endif
        @endforeach
    </div>
</div>

@endsection