<?php
use Illuminate\Routing\Redirector;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function (Redirector $redirect) {
    return $redirect->to('/home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/home', 'HomeController@select');

Route::post('/search', 'HomeController@search');

Route::get('/addmovie', 'AddmovieController@index')->middleware('admin');

Route::get('/cart', 'CartController@index')->middleware('internaute');

Route::get('/profil', 'ProfilController@index')->middleware('internaute');

Route::get('/movie/{id}', 'MovieController@show')->middleware('internaute');

Route::post('/movie/update', 'MovieController@update')->middleware('internaute');

Route::get('/movie/{id}/delete', 'MovieController@delete')->middleware('admin');

Route::get('/reset', 'CartController@reset');

Route::post('/addtodb', 'AddmovieController@insert');

Route::post('/addtocart','CartController@addToCart');

Route::get('/addorder', 'OrderController@addToOrder')->middleware('internaute');

Route::post('/render', 'OrderController@render');






